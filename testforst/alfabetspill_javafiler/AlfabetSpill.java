/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author grethsan
 */
import java.util.*;

class AlfabetSpill {
  public static void main(String[] args) {
	try{
		Kortstokk kortstokken = new Kortstokk();
		Spill spillet = new Spill(new Spiller("Grethe"), new Spiller("Nils"));
		String resultat = spillet.spillMangeOmganger(13);
		System.out.println(resultat + "\n" + spillet.avsluttSpill());
	} catch(Exception e){
		System.out.println(e);
	}

 }
} // AlfabetSpill

/* Kjøring av programmet

Grethe trakk K. Nils trakk Y.  De byttet ikke. Grethe har 1 p. og Nils har 0 p.
Grethe trakk M. Nils trakk R.  De byttet ikke. Grethe har 2 p. og Nils har 0 p.
Grethe trakk V. Nils trakk U.  De byttet. Grethe har 3 p. og Nils har 0 p.
Grethe trakk G. Nils trakk Q.  De byttet ikke. Grethe har 4 p. og Nils har 0 p.
Grethe vant :)
*/
