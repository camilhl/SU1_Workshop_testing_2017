/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author grethsan
 */
import java.util.*;

public class Kortstokk {
  private Random tallTrekker = new Random();
  public static final int forsteBokstav = (int) 'A';
  public static final int sisteBokstav = (int) 'Z';
  public int antallBokstaver = sisteBokstav - forsteBokstav+1;  // For å få med Z..(+1)
  ArrayList<Character> kortstokk = new ArrayList<Character>();

  public Kortstokk(){
      for (int i=forsteBokstav; i<=sisteBokstav; i++) {
	       kortstokk.add(new Character((char)i));
	  }
  }

  public ArrayList<Character> getKortstokk(){
	  return kortstokk;
  }
  public int getAntallBokstaver(){ 
      return antallBokstaver;
  }

  public char getKort() throws Exception{

	if (antallBokstaver > 0){
		int tall = tallTrekker.nextInt(antallBokstaver);
		char bokstav = kortstokk.get(tall);
		kortstokk.remove(tall);  // fjerner kort etterhvert som de brukes/ trekkes
		antallBokstaver--;
		return bokstav;
	} else throw new Exception("Ikke flere kort igjen"); // else sier dette er litt ufornuftig

 }
}

