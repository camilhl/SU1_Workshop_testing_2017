/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author grethsan
 */
public class SpillerTest {
    
    public SpillerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNavn method, of class Spiller.
     */
    @Test
    public void testGetNavn() {
        System.out.println("Spiller: getNavn");
        Spiller instance = new Spiller("Grethe");
        String expResult = "Grethe";
        String result = instance.getNavn();
        assertEquals(expResult, result);
    }

    /**
     * Test of getBokstav method, of class Spiller.
     */
    @Test
    public void testGetBokstav() {
        System.out.println("Spiller: getBokstav");
        Spiller instance =  new Spiller("Grethe");
        instance.setBokstav('A');
        char expResult = 'A';
        char result = instance.getBokstav();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAntallPoeng method, of class Spiller.
     */
    @Test
    public void testGetAntallPoeng() {
        System.out.println("Spiller: getAntallPoeng");
        Spiller instance =  new Spiller("Grethe");
        int expResult = 0;
        int result = instance.getAntallPoeng();
        assertEquals(expResult, result);
    }

    /**
     * Test of setBokstav method, of class Spiller.
     */
    @Test
    public void testSetBokstav() {
        System.out.println("Spiller: setBokstav");
        char nyBokstav = 'B';
        Spiller instance = new Spiller("Grethe");
        instance.setBokstav(nyBokstav);
        char expResult = 'B';
        char result = instance.getBokstav();
        assertEquals(expResult, result);
    }

    /**
     * Test of økAntallPoeng method, of class Spiller.
     */
    @Test
    public void testØkAntallPoeng() {
        System.out.println("Spiller: okAntallPoeng");
        Spiller instance = new Spiller("Grethe");
        instance.økAntallPoeng();
        instance.økAntallPoeng();
        instance.økAntallPoeng();
        instance.økAntallPoeng();
        int expResult = 4;
        int result = instance.getAntallPoeng();
        assertEquals(expResult, result);

    }

    /**
     * Test of toString method, of class Spiller.
     */
    @Test
    public void testToString() {
        System.out.println("Spiller: toString");
        Spiller instance = new Spiller("Grethe");
        instance.setBokstav('A');
        String expResult = "Grethe har bokstav: A og antall Poeng: 0";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
