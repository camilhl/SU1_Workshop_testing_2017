/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author grethsan
 */
public class KortstokkTest {
    private Kortstokk instance;
	private static final int forsteBokstav = 'A';
	private static final int sisteBokstav = 'Z';
    
    public KortstokkTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        		//lag ny kortstokken før hver test
		instance = new Kortstokk();
    }
    
    @After
    public void tearDown() {
    }

    
   @Test
   public void testGetKortstokk(){  // test om kortstokken inneholder alle bokstavene i alfabetet, og ingen duplikater
	   System.out.println("testGetKortstokk()");
	   ArrayList<Character> result = instance.getKortstokk();
	   ArrayList<Character> expResult = new ArrayList<Character>();
	   for(int i=forsteBokstav; i<=sisteBokstav; i++){
		   expResult.add(new Character((char)i));
	   }
	   assertEquals(expResult, result);
   }

   @Test
   public void testGetAntallBokstaver(){
	 System.out.println("testGetAntallBokstaver()");
	   int result = instance.getAntallBokstaver();
	   int expResult = sisteBokstav - forsteBokstav + 1;
	   assertEquals(expResult, result);
   }

   @Test
   public void testGetKort_normalsituasjon() throws Exception{ // tester at etter ett tall er trukket så er arraylisten med kort minket med 1
   	  System.out.println("testGetKort_normalsituasjon()");
   	  // trekker to kort
   	  char bokstav = instance.getKort();
   	  bokstav = instance.getKort();
   	  int result = instance.getAntallBokstaver();
   	  int expResult = sisteBokstav - forsteBokstav-1;  //trekker fra 1 for å få med Z..
   	  assertEquals(expResult, result);
   }

   @Test (expected=Exception.class)// tester at det kastes ett unntak når kortstokken er tom
   public void testGetKort_tomkortstokk() throws Exception{
   	    System.out.println("testGetKort_tomkortstokk()");
   	    int antallBokstaver = instance.getAntallBokstaver();
		for (int i=forsteBokstav; i<=sisteBokstav;i++){
                    char bokstav = instance.getKort();
		}
		char bokstav = instance.getKort(); // skal kaste unntak, kortstokken er nå tom
   }
    
}
